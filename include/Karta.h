#ifndef KARTA_H
#define KARTA_H

#include <iostream>
#include <string>

using namespace std;

class Karta {
public:
    int id;
    int figura;
    int kolor;

    Karta(int a, int b, int c): id(a), figura(b), kolor(c) {}
    Karta():id(0), figura(0), kolor(0) {}
};


#endif // KARTA_H
