
 // Damian Moskala i Kamil Kowalczyk, ATH - Informatyka - Semestr 2 - Grupa 3a.

#pragma endregion //Manipulacja konsola
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif

#include <iostream>
#include <string>
#include "windows.h" //Rysowanie kart
#include "time.h" //Tasowanie kart
#include <cstdio> //Tasowanie kart

using namespace std;

HANDLE hOut;
HWND console = GetConsoleWindow();

int wherex() //Zwraca polozenie X wskaznika
{
    CONSOLE_SCREEN_BUFFER_INFO sinf;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &sinf);
    return sinf.dwCursorPosition.X;
}

int wherey() //Zwraca polozenie Y wskaznika
{
    CONSOLE_SCREEN_BUFFER_INFO sinf;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &sinf);
    return sinf.dwCursorPosition.Y;
}

void gotoxy(const int x, const int y) //Ustawia wskaznik na wspolrzednych X,Y
{
    COORD coord = { x, y };
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

class Karta
{
public:
    int id;
    int figura;
    int kolor;

Karta(int a, int b, int c): id(a), figura(b), kolor(c){}
Karta():id(0), figura(0), kolor(0){}
};

Karta* tablica_kart[52];

void rysuj_piramide(int x, Karta * tablica_kart[])
{
    string tab[13]={ "2", "3", "4", "5", "6", "7", "8", "9", "10", "W", "D", "K", "A" };
    char tab2[4]={ (char)5, (char)6, (char)4, (char)3 };


    if(tab2[tablica_kart[x]->kolor]==(char)3 || tab2[tablica_kart[x]->kolor]==(char)4)
    {
        SetConsoleTextAttribute(hOut, 12+16*15); //Ustawia kolor czerwony dla kier i karo

        if(tab[tablica_kart[x]->figura]!="10") //Rysowanie kart dla wszystkich innych niz 10, ktora ma dwuznakowa wartosc
        {
            cout<<" ________ " ;
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|" << tab[tablica_kart[x]->figura] << tab2[tablica_kart[x]->kolor] << "      |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|________|";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"          ";

        }

        else //Rysowanie karty dla 10
        {
            cout<<" ________ " ;
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|" << tab[tablica_kart[x]->figura] << tab2[tablica_kart[x]->kolor] << "     |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"|________|";
            gotoxy(wherex()-10, wherey()+1);
            cout<<"          ";
        }
    }

    else
    {
        SetConsoleTextAttribute(hOut, 0+16*15); //Ustawia kolor czarny dla pik i trefl

        if(tab[tablica_kart[x]->figura]!="10") //Rysowanie kart dla wszystkich innych niz 10, ktora ma dwuznakowa wartosc
        {
            cout<<" ________ " ;
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|" << tab[tablica_kart[x]->figura] << tab2[tablica_kart[x]->kolor] << "      |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|________|";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"          ";
        }

        else //Rysowanie karty dla 10
        {
            cout<<" ________ " ;
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|"<<tab[tablica_kart[x]->figura] << "" <<tab2[tablica_kart[x]->kolor] << "     |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|        |";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"|________|";
            gotoxy(wherex()-10,wherey()+1);
            cout<<"          ";
        }
    }
}

void tasowanie(Karta* tablica_kart[])
{
Karta* tab1[52];
Karta* tab2[52];
Karta* tab3[52];
Karta* tab4[52];
Karta* tab5[52];

int k, wsk1=0, wsk2=0, wsk3=0, wsk4=0, wsk5=0, a1=0 ,a2=0 ,a3=0 ,a4=0 ,a5=0;
srand(time((NULL)));

    for (int ilosc = 0; ilosc<5; ilosc++)
    {
        for (int i = 0; i<52; i++)
        {
            k = rand() % 5 + 1;

            switch (k)
            {
            case 1:
                tab1[wsk1]=tablica_kart[i];
                wsk1++;
                break;

            case 2:
                tab2[wsk2]=tablica_kart[i];
                wsk2++;
                break;

            case 3:
                tab3[wsk3]=tablica_kart[i];
                wsk3++;
                break;

            case 4:
                tab4[wsk4]=tablica_kart[i];
                wsk4++;
                break;

            case 5:
                tab5[wsk5]=tablica_kart[i];
                wsk5++;
                break;
            }
        };

        for (int t = 0; t<52; t++)
        {
            if(a1<wsk1)
            {
                tablica_kart[t]=tab1[a1];
                a1++;
                t++;
            }

            if(a2<wsk2)
            {
                tablica_kart[t]<=tab2[a2];
                a2++;
                t++;
            }

            if(a3<wsk3)
            {
                tablica_kart[t]=tab3[a3];
                a3++;
                t++;
            }

            if(a4<wsk4)
            {
                tablica_kart[t]=tab4[a4];
                a4++;
                t++;
            }

            if(a5<wsk5)
            {
                tablica_kart[t]=tab5[a5];
                a5++;
                t++;
            }
        };
    };
}

void wyswietlanie_kart(Karta *tablica_kart[])
{
        int licznik=0;
        int licznik1=0;
        int licznik2=0;
        gotoxy(25,13);

        for (int x = 0; x < 3; x++) //Rysowanie pierwszego rzedu
        {
            if(x<3)
            {
			   SetConsoleTextAttribute(hOut, 0+16*15);
			   rysuj_piramide(x,tablica_kart);
               gotoxy(wherex()+60,wherey()-6);
            }
        }gotoxy(15,15);

        for (int x = 3; x < 9; x++) //Rysowanie drugiego rzedu
        {
            gotoxy(wherex()+3,wherey());
            rysuj_piramide(x,tablica_kart);
            gotoxy(wherex(),wherey()-6);

            int ax,ay;
            ax=wherex();
            ay=wherey();
            licznik++;

            if ((0==(licznik %2)) && licznik>0) //Odstep pomiedzy poszczegolnymi piramidami w drugim rzedzie
            {
            gotoxy(ax+44,ay);
            }
        }gotoxy(9,17);

        for (int x = 9; x < 18; x++) //Rysowanie trzeciego rzedu
        {
            gotoxy(wherex()+3,wherey());
            rysuj_piramide(x,tablica_kart);
            gotoxy(wherex(),wherey()-6);

            int ax,ay;
            ax=wherex();
            ay=wherey();
            licznik1++;

            if ((0==(licznik1 %3)) && licznik1>0) //Odstep pomiedzy poszczegolnymi piramidami w trzecim rzedzie
            {
                gotoxy(ax+31,ay);
            }
        }gotoxy(2,19);

		for (int x = 18; x < 30; x++) //Rysowanie czwartego rzedu
        {
            gotoxy(wherex()+3,wherey());
            rysuj_piramide(x,tablica_kart);
            gotoxy(wherex(),wherey()-6);

            int ax,ay;
            ax=wherex();
            ay=wherey();
            licznik2++;

            if ((0==(licznik2 %4)) && licznik2>0) //Odstep pomiedzy poszczegolnymi piramidami w czwartym rzedzie
            {
                gotoxy(ax+18,ay);
            }
        }gotoxy(3,30);

        for(int x=30;x<51;x++) //Rysowanie talii do pobierania
        {
            rysuj_piramide(x,tablica_kart);
            gotoxy(wherex()-9,wherey()-6);
        }

gotoxy(40,30);
rysuj_piramide(51,tablica_kart); //Rysowanie odkrytej karty
}

void stworz_talie()
{
    int y = 0;
    int z = 0;

    for (int x = 0; x < 52; x++)
    {
        Karta *nowa = new Karta(x,y,z);
		tablica_kart[x] = nowa;
        y++;

        if (y == 13)
        {
            y = 0;
            z++;
        }
    }
    tasowanie(tablica_kart);
    wyswietlanie_kart( tablica_kart);
}


void menu()
{
    char wybor;
    string imie;

    cout << "Witaj! Oto pasjans w wersji Tripeaks." << endl;
    start:
    cout << "1 - Graj!" << endl;
    cout << "2 - Opcje" << endl;
    cout << "3 - Zasady" << endl;
    cout << "4 - Autorzy" << endl;
    cout << "5 - Zakoncz" << endl << endl;
    cout << "Wybierz opcje: ";
    cin >> wybor;
    system("cls");

    switch(wybor)
    {
    case '1':
        cout << "Podaj swoje imie: ";
        cin >> imie;
        system("cls");

        cout << "PASJANS - TRIPEAKS" << endl << endl;
        cout << "Gracz: " << imie;

        stworz_talie();
        break;

    case '2':
        opcje:
        cout << "OPCJE" << endl << endl;
        cout << "Kolor konsoli:" << endl;
        cout << "1 - Zielony" << endl;
        cout << "2 - Morski" << endl;
        cout << "3 - Fioletowy" << endl;
        cout << "4 - Zloty" << endl;
        cout << "5 - Bialy" << endl << endl;
        cout << "Zmiana rozmiaru okna:" << endl;
        cout << "6 - Pelny ekran" << endl;
        cout << "7 - Okno" << endl << endl;
        cout << "8 - Powrot do menu" << endl;
        cout << endl << "Wybierz opcje: ";
        cin >> wybor;
        system("cls");

        switch (wybor)
        {
            case '1':
                system("COLOR 20");
                goto opcje;
                break;

             case '2':
                system("COLOR 30");
                goto opcje;
                break;

             case '3':
                system("COLOR 50");
                goto opcje;
                break;

             case '4':
                system("COLOR 60");
                goto opcje;
                break;

             case '5':
                system("COLOR F0");
                goto opcje;
                break;

             case '6':
                ShowWindow( console, SW_MAXIMIZE );
                goto opcje;
                break;

             case '7':
                ShowWindow( console, SW_RESTORE );
                goto opcje;
                break;

              case '8':
                goto start;
                break;

              default:
                cout << "Nie ma takiej opcji! Wybierz jeszcze raz :)" << endl << endl;
                goto opcje;
                break;
        }




    case '3':
        cout << "ZASADY GRY" << endl;
        cout << "1. Gra tworzy trzy piramidy kart z jednej, przetasowanej talii. W kazdej piramidzie odkryty jest tylko czwarty, najnizszy rzad kart." << endl;
        cout << "2. Ponizej piramid jest reszta talii oraz jedna odkryta karta." << endl;
        cout << "3. Gracz moze przekladac karty z piramid na odkryta karte lub odkrywac nastepna karte z reszty talii." << endl;
        cout << "4. Po przelozeniu calego rzedu kart piramidy, odkryty zostaje nastepny rzad." << endl;
        cout << "5. Aby moc przelozyc karte z piramidy na odkryta karte, musza byc one w relacji wyzsza na nizsza lub odwrotnie." << endl;
        cout << "6. Oto wartosci kart od najnizszej do najwyzszej: 2 < 3 < 4 < 5 < 6 < 7 < 8 < 9 < 10 < W < D < K < A." << endl;
        cout << "7. Kolor nie wplywa na mozliwosc przelozenia kart." << endl;
        cout << "8. Gracz wygrywa kiedy wszystkie karty beda na jednym stosie, gdzie poczatkowo byla jedna odkryta karta." << endl;
        cout << "9. Gracz przegrywa kiedy nie ma mozliwosci przelozenia kart, a reszta talii zostala przelozona na stos odkrytych kart." << endl;
        cout << endl << "Milej zabawy! :)" << endl << endl;
        goto start;
        break;

    case '4':
        cout << "AUTORZY " << endl;
        cout << "> Damian Moskala - wyglad oraz mechanika gry" << endl;
        cout << "> Kamil Kowalczyk - mechanika gry" << endl << endl;
        cout << "> Specjalne podziekowania dla:" << endl;
        cout << "Kamila Kubicy za pomoc przy pracy nad kodem." << endl << endl;
        goto start;
        break;

    case '5':
        exit(0);
        break;

    default:
        cout << "Nie ma takiej opcji! Wybierz jeszcze raz :)" << endl << endl;
        goto start;
        break;
    }
}

int main()
{
    hOut = GetStdHandle(STD_OUTPUT_HANDLE); //Wskaznik

    system("COLOR 30"); //Tlo konsoli
    SetConsoleTextAttribute(hOut, 0+16*18);

    COORD c = {200,60}; //Rozmiar okna
    SetConsoleScreenBufferSize( GetStdHandle( STD_OUTPUT_HANDLE ), c ); //Rozmiar bufora
    ShowWindow( console, SW_MAXIMIZE ); //Pelny ekran

    menu();

    gotoxy(2,40); //Przesuwa napisy na koniec planszy

    return 0;
}
